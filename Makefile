JC=javac
JFLAGS=-g -cp ini4j-0.5.4.jar:.

.SUFFIXES: .java .class
.PHONY: clean

CLASSES= \
	Novel.java \
	Story.java
#	Dialogue.java

classes: $(CLASSES:.java=.class)

.java.class:
	$(JC) $(JFLAGS) $*.java

clean:
	$(RM) *.class
