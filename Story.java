import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import org.ini4j.Ini;

public class Story extends JPanel {
	private Novel novel;

	/**
	 *
	 */
	public Story(Novel novel, Ini ini) {
		try {
			parseFile(ini.get("system").get("story"));
		}
		catch (IOException e) {
			e.printStackTrace();
			System.out.println("Unable to find the story file!");
			System.exit(1);
		}
	}

	/**
	 * Reads each line in given file.
	 *
	 * @param file Name of file to read
	 *
	 * @throws IOException
	 */
	public void parseFile(String file) throws IOException {
		Files.lines(new File(file).toPath()).forEachOrdered(this::parseLine);
	}

	/**
	 * Reads given line and calls corresponding method.
	 *
	 * @param line String to parse
	 */
	public void parseLine(String line) {
		line = line.trim();

		// Skip empty lines & comments
		if (line.isEmpty() || line.startsWith("#")) {
			return;
		}

		// Identify line content & call corresponding method
		String[] parts = line.toLowerCase().split("\\s+", 2);

		if (parts[0].equals("id")) {
			System.out.println("Selects '" + parts[1] + "'");
			//currentId = parts[1];
		}
		else if (parts[0].equals("show")) {
			System.out.println("Shows @ '" + parts[1] + "'");
		}
		else if (parts[0].equals("scene")) {
			System.out.println("Scene '" + parts[1] + "'");
			//currentScene = ini.get("Scenes").get(parts[1]);
			//novel.repaint();
		}
		else if (parts[0].equals("story")) {
			System.out.println("Current story: '" + parts[1] + "'");
		}
		else {
			//slowPrint(line);
		}
	}

	/**
	 * Prints given string slwoly.
	 *
	 * @param line Line to print
	 */
	public void slowPrint(String line) {

	}
}
