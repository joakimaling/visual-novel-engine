import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JTextArea;
import javax.swing.Timer;

public class Dialogue extends JTextArea {
	private String message;
	private Timer timer;
	private int counter;

	public Dialogue() {
		super();

		setFocusable(false);
		setEditable(false);

		// The timer handles the slow-printing by outputting the next character
		// in the message each interval. When the counter matches the length of
		// the massage the timer is stopped
		timer = new Timer(100, new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				append(String.valueOf(message.charAt(counter++)));
				setCaretPosition(getDocument().getLength());
				if (counter >= message.length()) {
					timer.stop();
				}
			}
		});
	}

	/**
	 * Prints given string slwoly.
	 *
	 * @param message Text to print.
	 */
	public void slowPrint(String message) {
		this.message = message;
		counter = 0;
		setText(null);
		timer.start();
	}
}
