import java.awt.Color;
import java.awt.Graphics;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import org.ini4j.Ini;

public class Novel extends JFrame {
	private Ini ini = null;

	/**
	 *
	 */
	public Novel() {
		getSystemFileContent();

		//
		getContentPane().add(new Story(this, ini));

		//
		setIconImage(new ImageIcon(ini.get("system").get("icon")).getImage());
		setLocationRelativeTo(null);
		setResizable(false);
		setSize(800, 600);
		setTitle(ini.get("system").get("title"));
		setVisible(true);
	}

	/**
	 *
	 */
	private void getSystemFileContent() {
		try {
			ini = new Ini(new FileReader("./system.ini"));
		}
		catch (IOException e) {
			e.printStackTrace();
			System.out.println("Unable to find/parse './system.ini' file!");
			System.exit(1);
		}
	}

	/**
	 *
	 */
	public void paint(Graphics g) {
		try {
			g.drawImage(ImageIO.read(new File("./graphics/scenes/beach.jpg")), 0, 0, null);
		}
		catch (IOException e) {
			g.setColor(Color.BLACK);
			g.fillRect(0, 0, getWidth(), getHeight());
		}
	}

	/**
	 *
	 */
	public static void main(String[] args) {
		new Novel();
	}
}
